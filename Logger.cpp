/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include <cassert>

#include "Logger.h"

Logger Logger::instance_;

void Logger::log(LogLevel level, std::string && message)
{
    if (level <= instance_.maxLogLevel_)
    {
        LogEntry entry(level, std::move(message));
        for (auto &method : instance_.methods_)
            method->log(instance_.id_, entry);
    }
}

void Logger::addLogMethod(std::unique_ptr<LogMethod> method)
{
    instance_.methods_.emplace_front(std::move(method));
}

void Logger::setMaxLogLevel(LogLevel level)
{
    // Assert is a noop when not debug so dont bother ifdef'ing the check for
    // max info in the assertion...
    assert(LogLevel::Emergency <= level && level <= LogLevel::Debug);
    instance_.maxLogLevel_ = level;
}

void Logger::setLogID(const std::string & id)
{
    instance_.id_ = id;
}

Logger::Logger() :
    id_(),
#if !defined(NDEBUG)
    maxLogLevel_(LogLevel::Debug)
#else
    maxLogLevel_(LogLevel::Warning)
#endif
{
}

Logger::~Logger()
{
}
