/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include <fmt/format.h>
#include <iomanip>
#include <sstream>

#include "WebSocketLogMethod.h"

static int callback_wsi_log(struct lws *wsi, enum lws_callback_reasons reason, void *user, void *in, size_t len);

static const struct lws_protocols protocols[] =
{
	{
		"log-source",
		callback_wsi_log,
		0, 0, 0, nullptr, 0
	},
	{ NULL, NULL, 0, 0, 0, nullptr, 0 } /* end */
};

struct WebSocketLogMethodInternal
{
    struct lws * wsi_log_;
    const std::string * id_;
    const LogEntry * entry_;
    struct lws_context * ctx_;
	struct lws_context_creation_info ctx_info_;
    struct lws_client_connect_info con_info_;
    const char * prot_;
    const char * p_;
};

WebSocketLogMethod::WebSocketLogMethod() :
    port_(0),
    addr_("localhost"),
    internal_(nullptr)
{
    internal_ = new WebSocketLogMethodInternal;
    memset(internal_, 0, sizeof(WebSocketLogMethodInternal));
}

WebSocketLogMethod::~WebSocketLogMethod()
{
}

void WebSocketLogMethod::log(const std::string & id, const LogEntry & entry)
{
    // TODO Revisit this... just trying to get it to mininally work at the moment

    internal_->id_ = &id;
    internal_->entry_ = &entry;

    if (!isConnected())
        connect();

    if (isConnected())
    {
        lws_callback_on_writable_all_protocol(internal_->ctx_, &protocols[0]);
        // NOTE: This may not hold true for cross platform code, but the
        // timeout 0 looks like it goes down to a 0 values timeval used in a
        // select which should be nonblocking.
        lws_service(internal_->ctx_, 0);
    }

    internal_->id_ = nullptr;
    internal_->entry_ = nullptr;
}

bool WebSocketLogMethod::createContext()
{
    auto & i = internal_->ctx_info_;

    i.port = CONTEXT_PORT_NO_LISTEN;
    i.protocols = protocols;
    i.gid = -1;
    i.uid = -1;

	if (!(internal_->ctx_ = lws_create_context(&i)))
        lwsl_err("log: failed to create context\n");

    return haveContext();
}

bool WebSocketLogMethod::haveContext()
{
    return internal_->ctx_ != nullptr;
}

bool WebSocketLogMethod::connect()
{
    auto & i = internal_->con_info_;

    lwsl_notice("log: connecting\n");

    i.port = port_;
    if (lws_parse_uri(const_cast<char *>(addr_.c_str()), &internal_->prot_, &i.address, &i.port, &internal_->p_) ||
            (!haveContext() && !createContext()))
    {
        lwsl_err("log: connection failure\n");
    }
    else
    {
        i.context = internal_->ctx_;
        i.host = i.address;
        i.origin = i.address;
        i.protocol = protocols[0].name;
        i.pwsi = &internal_->wsi_log_;
        i.path = internal_->p_;
        i.userdata = internal_;

        lws_client_connect_via_info(&i);
    }

    return isConnected();
}

bool WebSocketLogMethod::isConnected()
{
    return internal_->wsi_log_ != nullptr;
}

static int callback_wsi_log(
        struct lws *wsi,
        enum lws_callback_reasons reason,
        void *user,
        void *in,
        size_t len __attribute__ ((unused)))
{
    WebSocketLogMethodInternal * i = user ? static_cast<WebSocketLogMethod *>(user)->internal_ : nullptr;

    switch (reason)
    {
        // TODO I think if we add a dummy first protocall, these will go to the
        // first protocol
        case LWS_CALLBACK_GET_THREAD_ID:
        case LWS_CALLBACK_CHANGE_MODE_POLL_FD:
        case LWS_CALLBACK_LOCK_POLL:
        case LWS_CALLBACK_UNLOCK_POLL:
            // Ignored
            break;

        case LWS_CALLBACK_CLIENT_FILTER_PRE_ESTABLISH:
            lwsl_err("log: LWS_CALLBACK_CLIENT_FILTER_PRE_ESTABLISH: %s\n", in ? (char *)in : "(null)");
            break;

        case LWS_CALLBACK_CLIENT_ESTABLISHED:
            lwsl_info("log: LWS_CALLBACK_CLIENT_ESTABLISHED\n");
            break;

        case LWS_CALLBACK_CLOSED:
            lwsl_notice(fmt::format("log: {} LWS_CALLBACK_CLOSED\n", *i->id_).c_str());
            i->wsi_log_ = nullptr;
            break;

	    case LWS_CALLBACK_CLIENT_CONNECTION_ERROR:
            lwsl_err("log: CLIENT_CONNECTION_ERROR: %s\n", in ? (char *)in : "(null)");
            break;

        case LWS_CALLBACK_CLIENT_RECEIVE:
            lwsl_err(fmt::format("log: {} unexpected LWS_CALLBACK_CLIENT_RECEIVE\n", *i->id_).c_str());
            break;

        case LWS_CALLBACK_CLIENT_WRITEABLE:
            {
                auto i = static_cast<WebSocketLogMethodInternal *>(lws_wsi_user(wsi));
                auto & id = *i->id_;
                auto & entry = *i->entry_;
                struct tm t;
                gmtime_r(&entry.date_, &t);
                   fmt::MemoryWriter w;
                   w.write("{}|{}-{}-{} {}:{}:{}|{} ms|{}|{}",
                           id,
                           t.tm_year + 1900, t.tm_mon, t.tm_mday,
                           t.tm_hour, t.tm_min, t.tm_sec,
                           std::chrono::duration_cast<std::chrono::milliseconds>(entry.monotonic_.time_since_epoch()).count(),
                           entry.logLevelString(),
                           entry.message_);
                //lws_write(wsi, (unsigned char *)w.data(), w.size(), LWS_WRITE_TEXT);

                unsigned char buffer[4096 + LWS_PRE];
                unsigned char * p = buffer + LWS_PRE;
                size_t len = std::min((size_t)4095,w.size());
                strncpy((char *)p, w.c_str(), 4096);
                lws_write(wsi, p, len, LWS_WRITE_TEXT);
            }
            break;

        default:
            lwsl_notice(fmt::format("log: Unhandled callback reason {}\n", reason).c_str());
            break;
    }

    return 0;
}
