/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef LOGENTRY_H
#define LOGENTRY_H

#include <chrono>
#include <string>
#include <type_traits>

#include "LogLevel.h"

struct LogEntry
{
    LogEntry() = delete;

#if 0
    LogEntry(const LogEntry &obj)
    {
        // TODO
    }

    LogEntry(LogEntry &&obj)
    {
        // TODO
    }
#endif

    LogEntry(LogLevel level, std::string &&message) :
        level_(level),
        message_(std::move(message)),
        date_(std::chrono::system_clock::to_time_t(std::chrono::system_clock::now())),
        monotonic_(std::chrono::steady_clock::now())
    {
    }

    std::string logLevelString() const;

    // TODO Just make these private and turn this into a class
    const LogLevel level_;
    const std::string message_;
    const time_t date_;
    const std::result_of<decltype(&std::chrono::steady_clock::now)()>::type monotonic_;
};

std::ostream & operator<<(std::ostream & os, const LogEntry & entry);

#endif
