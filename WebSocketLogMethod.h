/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef WEBSOCKETLOGMETHOD_H
#define WEBSOCKETLOGMETHOD_H

#include "libwebsockets.h"

#include "LogMethod.h"

struct WebSocketLogMethodInternal;

class WebSocketLogMethod : public LogMethod
{
    public:
        WebSocketLogMethod();
        virtual ~WebSocketLogMethod();

        virtual void log(const std::string & id, const LogEntry & entry);

        uint16_t port_;
        std::string addr_;

        WebSocketLogMethodInternal * internal_;

    private:
        bool createContext();
        bool haveContext();
        bool connect();
        bool isConnected();
};

#endif
