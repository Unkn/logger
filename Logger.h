/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef LOGGER_H
#define LOGGER_H

#include <forward_list>
#include <memory>
#include <string>

#include <fmt/format.h>
// This enables std::string_view to be used in the format args
#include <fmt/ostream.h>

#include "LogEntry.h"
#include "LogMethod.h"

#if !defined(OMIT_LOGGING)
#define LOG_EMERG(msg...)     Logger::log(LogLevel::Emergency, fmt::format(msg))
#define LOG_ALERT(msg...)     Logger::log(LogLevel::Alert, fmt::format(msg))
#define LOG_CRIT(msg...)      Logger::log(LogLevel::Critical, fmt::format(msg))
#define LOG_ERROR(msg...)     Logger::log(LogLevel::Error, fmt::format(msg))
#define LOG_WARN(msg...)      Logger::log(LogLevel::Warning, fmt::format(msg))
#define LOG_NOTICE(msg...)    Logger::log(LogLevel::Notice, fmt::format(msg))
#define LOG_INFO(msg...)      Logger::log(LogLevel::Informational, fmt::format(msg))
#else
#define LOG_EMERG(msg...)
#define LOG_ALERT(msg...)
#define LOG_CRIT(msg...)
#define LOG_ERROR(msg...)
#define LOG_WARN(msg...)
#define LOG_NOTICE(msg...)
#define LOG_INFO(msg...)
#endif

#if !defined(NDEBUG) && !defined(OMIT_LOGGING)
#define LOG_DEBUG(msg...)     Logger::log(LogLevel::Debug, fmt::format(msg))
#else
#define LOG_DEBUG(msg...)
#endif

class Logger
{
    public:
        // TODO Use the buffer thing that is used by fmt to not
        // create a string unecessarily
        static void log(LogLevel level, std::string &&message);
        static void addLogMethod(std::unique_ptr<LogMethod> method);
        static void setMaxLogLevel(LogLevel level);
        static void setLogID(const std::string & id);

    private:
        Logger();
        ~Logger();

        // Expected to be the only singleton... but that shouldn't matter since
        // by it's design it won't interfere with unit testing
        static Logger instance_;

        std::string id_;
        LogLevel maxLogLevel_;
        // TODO May want to toggle log methods for whatever reason...
        std::forward_list<std::unique_ptr<LogMethod> > methods_;
};

#endif
