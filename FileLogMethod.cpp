/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include <string>
#include <cstdlib>

#include "FileLogMethod.h"

// TODO Just hardcoding a 4MiB limit for the log files for now, but it should
// be configurable
const std::ofstream::pos_type FileLogMethod::MAX_LOG_SIZE = (1024 * 1024 * 4);
const std::experimental::filesystem::v1::path FileLogMethod::EXTENSION(".log");

FileLogMethod::FileLogMethod()
{
}

FileLogMethod::~FileLogMethod()
{
}

bool FileLogMethod::setLogFile(const std::experimental::filesystem::v1::path &logFile)
{
    logFile_ = logFile;
    of_.close();
    return openLogFile();
}

void FileLogMethod::log(const LogEntry &entry)
{
    bool ok = of_.is_open() && rotateLogs();
    if (ok)
    {
        // Don't use endl, we dont want flushing to cause any delays
        of_ << entry << "\n";
    }
}

bool FileLogMethod::openLogFile()
{
    of_.open(getCurrentLogFile());
    return of_.is_open();
}

std::experimental::filesystem::v1::path FileLogMethod::getCurrentLogFile() const
{
    auto currentFile = logFile_;
    currentFile += EXTENSION;
    return currentFile;
}

std::experimental::filesystem::v1::path FileLogMethod::getNextRotatedLogFile() const
{
    auto idx = std::to_string(getNextLogFileIndex());
    auto dot = std::string(1, '.');
    auto rotatedLog = logFile_;

    rotatedLog += dot;
    rotatedLog += idx;
    rotatedLog += EXTENSION;

    return rotatedLog;
}

bool FileLogMethod::isFull()
{
    return of_.tellp() >= MAX_LOG_SIZE;
}

bool FileLogMethod::rotateLogs()
{
    bool ok = true;

    if (isFull())
    {
        of_.close();
        std::experimental::filesystem::v1::rename(getCurrentLogFile(), getNextRotatedLogFile());
        ok = openLogFile();
    }

    return ok;
}

int FileLogMethod::getNextLogFileIndex() const
{
    // TODO Can do some of this in setLogFile
    int idx = -1;

    for (auto & p : std::experimental::filesystem::v1::directory_iterator(logFile_.parent_path()))
    {
        // Rotated log files will be of the form BASENAME.INDEX.EXTENSION

        auto currPath = p.path();

        if (EXTENSION.compare(currPath.extension()) != 0)
            continue;

        // Grab what we expect to be an index from the path
        auto tmpPath = currPath.stem();
        std::string tmp = tmpPath.extension().string();
        if (tmp.empty() || logFile_.filename().compare(tmpPath.stem()) == 0)
            continue;

        char *valid;
        int newIdx = strtol(tmp.c_str(), &valid, 10);
        if (*valid != '\0')
            continue;

        if (idx < newIdx)
            idx = newIdx;
    }

    return idx + 1;
}
