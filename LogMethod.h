/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef LOGMETHOD_H
#define LOGMETHOD_H

#include <string>

#include "LogEntry.h"

class LogMethod
{
    public:
        virtual void log(const std::string & id, const LogEntry & entry) = 0;
};

#endif
