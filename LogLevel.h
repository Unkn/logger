/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef LOGLEVEL_H
#define LOGLEVEL_H

/**
 * Syslog severity levels
 *
 * @see Section 6.2.1 Table 2. https://tools.ietf.org/html/rfc5424
 */
enum class LogLevel
{
    Emergency,
    Alert,
    Critical,
    Error,
    Warning,
    Notice,
    Informational,
    Debug
};

#endif
